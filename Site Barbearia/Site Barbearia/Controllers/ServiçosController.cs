﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Site_Barbearia.Controllers
{
    public class ServiçosController : Controller
    {
        // GET: Serviços
        public ActionResult Index()
        {
            return View();
        }

        Business.BsServiços.BusinessServiços businessServiços = new Business.BsServiços.BusinessServiços();

        public ActionResult NovoAtendimento()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NovoAtendimento(Database.Entity.tb_servico model)
        {
            businessServiços.NovoAtendimento(model);

            return View();
        }

        public ActionResult ConsultarServiço()
        {
            List<Database.Entity.tb_servico> lista = businessServiços.ListarServiços();

            return View(lista);
        }

        /*[HttpPost]
        public ActionResult ConsultarServiço(Database.Entity.tb_servico model)
        {
            List<Database.Entity.tb_servico> lista = businessServiços.ListarServiços();

            return View(lista);
        }*/

        public ActionResult AlterarServiço()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AlterarServiço(Database.Entity.tb_servico model)
        {
            businessServiços.AlterarServiço(model);

            return View();
        }


        public ActionResult RemoverServiço()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RemoverServiço(Database.Entity.tb_servico model)
        {
            businessServiços.RemoverServiço(model);

            return View();
        }
    }
}