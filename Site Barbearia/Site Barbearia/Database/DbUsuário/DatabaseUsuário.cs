﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site_Barbearia.Database.DbUsuário
{
    public class DatabaseUsuário
    {
        Entity.mydbEntities db = new Entity.mydbEntities();

        public bool EfetuarLogin(string usuario, string senha)
        {
            bool contem = db.tb_usuario
                                           .Any(x => x.nm_usuario == usuario && x.nm_senha == senha);

            return contem;
        }

        public bool UsuarioExistente(string usuario)
        {
            //Verifica se já existe esse usuário
            bool contem = db.tb_usuario
                                           .Any(x => x.nm_usuario == usuario);

            return contem;
        }

        public void Cadastro(Entity.tb_usuario usuario)
        {
            db.tb_usuario.Add(usuario);
            db.SaveChanges();
        }

        public Entity.tb_usuario ConsultarPorUsuario(string nome)
        {
            Entity.tb_usuario model = db.tb_usuario.FirstOrDefault(x => x.nm_usuario == nome);
            return model;
        }

        public void AlterarUsuario(Entity.tb_usuario usuario)
        {
            Entity.tb_usuario alterar = db.tb_usuario.First(x => x.nm_usuario == usuario.nm_usuario);
            alterar.nm_senha = usuario.nm_senha;
            db.SaveChanges();
        }


        public List<Entity.tb_usuario> ListarUsuarios()
        {
            List<Entity.tb_usuario> lista = db.tb_usuario.ToList();
            return lista;
        }

        public void AdicionarCodigo(string codigo, string usuario)
        {
            Entity.tb_usuario alterar = db.tb_usuario.First(x => x.nm_usuario == usuario);
            alterar.ds_codigo_alteracao = codigo;
            db.SaveChanges();
        }

        public void LimparCodigo(string usuario)
        {
            Entity.tb_usuario alterar = db.tb_usuario.First(x => x.nm_usuario == usuario);
            alterar.ds_codigo_alteracao = string.Empty;
            db.SaveChanges();
        }

        public Entity.tb_usuario ModeloUsuarioAtivo(string usuario)
        {
            Entity.tb_usuario lista = db.tb_usuario.
                                                    FirstOrDefault(x => x.nm_usuario == usuario);

            return lista;
        }

        public void AlterarSenha(string usuario, string senha, string codigo)
        {
            Entity.tb_usuario alterar = db.tb_usuario.First(x => x.nm_usuario == usuario &&
                                                                                x.ds_codigo_alteracao == codigo);
            alterar.ds_codigo_alteracao = "";
            alterar.nm_senha = senha;

            db.SaveChanges();
        }

        public bool VerificarCodigo(string usuario, string codigo)
        {
            return db.tb_usuario.Any(x => x.nm_usuario == usuario &&
                                                         x.ds_codigo_alteracao == codigo);
        }

        public void RemoverUsuario(Entity.tb_usuario usuario)
        {
            Entity.tb_usuario remover = db.tb_usuario.First(x => x.nm_usuario == usuario.nm_usuario);
            db.tb_usuario.Remove(remover);
            db.SaveChanges();
        }

        public void RemoverUsuarioPorID(int id)
        {
            Entity.tb_usuario remover = db.tb_usuario.First(x => x.id_funcionario == id);
            db.tb_usuario.Remove(remover);
            db.SaveChanges();
        }
    }
}