﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site_Barbearia.Database.DbServiços
{
    public class DatabaseServiços
    {
        Entity.mydbEntities db = new Entity.mydbEntities();

        public void NovoAtendimento(Entity.tb_servico servico)
        {
            db.tb_servico.Add(servico);
            db.SaveChanges();
        }

        public List<Entity.tb_servico> ListarServiços()
        {
            List<Entity.tb_servico> lista = db.tb_servico.ToList();
            return lista;
        }

        public Entity.tb_servico ConsultarPorID(int id)
        {
            Entity.tb_servico model = db.tb_servico.FirstOrDefault(x => x.id_servico == id);
            return model;
        }

        public List<Entity.tb_servico> Consultar_ID(int id)
        {
            List<Entity.tb_servico> model = db.tb_servico.Where(x => x.id_servico == id).ToList();
            return model;
        }

        public void AlterarServiço(Entity.tb_servico model)
        {
            Entity.tb_servico alterar = db.tb_servico.First(x => x.id_servico == model.id_servico);
            alterar.nm_responsavel_servico = model.nm_responsavel_servico;
            alterar.id_corte = model.id_corte;
            alterar.vl_servico = model.vl_servico;
            alterar.dt_servico = model.dt_servico;
            alterar.hr_servico = model.hr_servico;
            db.SaveChanges();
        }

        public bool VerificarServiço(int id)
        {
            bool contem = db.tb_servico.Any(x => x.id_servico == id);
            return contem;
        }

        public void RemoverServiço(Entity.tb_servico model)
        {
            bool contem = VerificarServiço(model.id_servico);
            if (contem == true)
            {
                Entity.tb_servico remover = db.tb_servico.First(x => x.id_servico == model.id_servico);
                db.tb_servico.Remove(remover);
                db.SaveChanges();
            }
        }

        public void RemoverServiçoPorID(int id)
        {
            bool contem = VerificarServiço(id);
            if (contem == true)
            {
                Entity.tb_servico remover = db.tb_servico.First(x => x.id_servico == id);
                db.tb_servico.Remove(remover);
                db.SaveChanges();
            }
        }

        public bool ServiçoExistente(TimeSpan serviço, DateTime data)
        {
            //Verifica se já existe esse serviço
            bool contem = db.tb_servico
                                           .Any(x => x.hr_servico == serviço && x.dt_servico == data);

            return contem;
        }
    }
}