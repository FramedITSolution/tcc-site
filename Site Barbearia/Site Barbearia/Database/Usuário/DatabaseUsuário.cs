﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site_Barbearia.Database.Usuário
{
    public class DatabaseUsuário
    {
        Entity.mydbEntities db = new Entity.mydbEntities();

        public void NovoLog(Entity.tb_log log)
        {
            db.tb_log.Add(log);
            db.SaveChanges();
        }

        public bool UsuarioPossuiLog(int usuario)
        {
            //Verifica se esse usuário possui log
            bool contem = db.tb_log
                                           .Any(x => x.id_usuario == usuario);

            return contem;
        }

        public void RemoverLog(int id)
        {
            bool contem = UsuarioPossuiLog(id);
            while (contem == true)
            {
                Entity.tb_log remover = db.tb_log.First(x => x.id_usuario == id);
                db.tb_log.Remove(remover);
                db.SaveChanges();
                contem = UsuarioPossuiLog(id);
            }
        }
    }
}